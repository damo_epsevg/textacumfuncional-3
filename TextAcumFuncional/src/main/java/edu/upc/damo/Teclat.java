package edu.upc.damo;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;

abstract class Teclat {
    final static int  DESCONEGUT = 0;
    final static int  OK = 1;
    abstract int accio( int actionId, KeyEvent event);


    static class TeclatHard extends Teclat{
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

    static class TeclatSoft extends Teclat{
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

}

